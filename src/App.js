import React from 'react';
import logo from './logo.svg';
import './App.css';
import BootstrapTableExample from './BootstrapTableExample';

function App() {
  return (
    <div className="App">
        <BootstrapTableExample />
    </div>
  );
}

export default App;
