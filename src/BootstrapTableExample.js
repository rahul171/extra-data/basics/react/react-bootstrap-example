import React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';

export default class BootstrapTableExample extends React.Component {
    rowEvents = {
        onClick: async (e, row, rowId) => {
            console.log(this);
            console.log(e);
            console.log(row);
            console.log(rowId);
            await new Promise((resolve, reject) => {
                setTimeout(() => {
                    resolve();
                }, 3000);
            })
        },
    };

    render() {
        const data = [
            {
                id: '1',
                name: 'name1',
            },
            {
                id: '2',
                name: 'name2',
            },
        ];

        const columns = [
            {
                dataField: 'id',
                text: 'ID',
            },
            {
                dataField: 'name',
                text: 'Name',
            },
        ];

        const data1 = [
            {
                id1: '11',
                name1: 'name11',
            },
            {
                id1: '12',
                name1: 'name12',
            },
        ];

        const columns1 = [
            {
                dataField: 'id1',
                text: 'ID1',
            },
            {
                dataField: 'name1',
                text: 'Name1',
            },
        ];

        const expandRow = {
            renderer: (row) => {
                console.log('expandRow =>', row);

                return (
                    <div>
                        <BootstrapTable keyField="id1" data={data1} columns={columns1} />
                    </div>
                );
            },
        };

        return (
            <BootstrapTable
                keyField="id"
                data={data}
                columns={columns}
                striped
                rowEvents={this.rowEvents}
                expandRow={expandRow}
            />
        );
    }
}
